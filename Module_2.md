# Module 2

## Création de compte

- Formulaire personnalisé par n° de lot (version papier + numérique)

- Calcul des subventions pour les rénovations

→ définition d’un User unique qui peut remplir le formulaire /voir le modifier 
→ création d'une page (accessible uniquement par lui et/ou l’admin) 
     qui permet de visualiser ces informations.

#### Questionnaire IF.pdf

Qu'est-ce qu'il nous faut ?


- Créer un formulaire
- Une BDD utilisateur
- Créer une connexion entre l'utilisateur et la BDD (interface restrictive) 
- Relation entre lot et user à voir (mail, lot, tél, numéro ID intermédiaire)



Relation entre table User et table des Lots

