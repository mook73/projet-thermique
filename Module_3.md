# Module 3


## Module 3 : les données liées au scénario
La totalité de ces éléments sont paramétrables depuis l'interface admin et permettent de croiser les données liées à l'immeuble, au diagnostic énergétique et de proposer les travaux envisagés avec leur coût. Voir document "Données techniques d'entrée.docx"


- A quoi ça sert ? :

Paramétrer toutes les données pour aboutir à un scénario, à rentrer depuis l'interface administrateur. 
Les données prises en compte :

- Les données des immeubles
- Le diagnostic énergétique
- Les coûts des travaux

Comment le faire ? :

- => Un formulaire admin par type de données (2 types ?) (forme CRUD)
- => Une relation entre les données et les travaux (calculs  ? algos ?)
- => Une sortie scénario (travaux)

#### Données techniques d'entrée.docx

Questions :

Générateur de scénario ? Ou Fait à la main à partir des données ?
Algos / formules fournies ?




