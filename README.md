# Projet Thermique

## Chef de projet : 

Sofien

## Bînomes :

- Maxence, Sofien (modules 1 et 2 )
- Nacime, Pascaline (modules 3 et 4)

## Responsable :

- Maxence : 1
- Sofien : 2
- Pascaline : 3
- Nacime : 4 / 5

## Normes :

- Tout en anglais.
- Camel case pour les variables.
- Nom de dossiers & fichiers : Majuscule au début puis camelcase.(upper camel case)
- Noms de classe : uppercamelcase.
- Commentaires (en anglais)
- Faire des commits avec des noms pertinents.


## Backlog :

### Objectif semaine 1 :

1. Définition des entités, puis les tables (distinction notamment due à la présence de tables intermédiaires)
2. Travail sur les modules (technos, description, schéma UML, wireframe)

Chaque personne responsable du module travaille dessus.


### Objectif semaine 2 :


 

